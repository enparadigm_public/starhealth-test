/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

// Wait for the deviceready event before using any of Cordova's device APIs.
// See https://cordova.apache.org/docs/en/latest/cordova/events/events.html#deviceready
var initialData = true;
var app = {
    // Application Constructor
    initialize: function () {
        document.addEventListener('deviceready', this.onDeviceReady.bind(this), false);
    },

    // deviceready Event Handler
    //
    // Bind any cordova events here. Common events are:
    // 'pause', 'resume', etc.
    onDeviceReady: function () {
        this.receivedEvent('deviceready');
    },

    // Update DOM on a Received Event
    receivedEvent: function (id) {
        console.log('Received Event: ' + id);
        document.getElementById("login").addEventListener("click", login);
        document.getElementById("smart_sell").addEventListener("click", smartSell);
        document.getElementById("smart_skill").addEventListener("click", smartSkill);
        document.getElementById("log_out").addEventListener("click", logout);
        // document.getElementById("initialize").addEventListener("click", initialize);
        // document.getElementById("is_user_initialized").addEventListener("click", isUserInitialised);
        hideButtons();



        var firebasePlugin = window.FirebasePlugin;

        firebasePlugin.getToken(function (token) {
            console.log("line ---> getToken1 : " + token);
        }, function (error) {
            console.log("line ---> getToken2 : " + error);
        });

        window.FirebasePlugin.onTokenRefresh(function (token) {
            console.log("line ---> onTokenRefresh1 : " + token);
            
            var plugin = new SmartSellPlugin();
            plugin.isUserInitialised("9999999999",
                function (msg) {  
                    plugin.saveFcmToken(token,
                        function (succMsg) {
                            console.log("line ---> onTokenRefresh : success");
                        },
                        function (err) {
                            console.log("line ---> onTokenRefresh : fail");
                        });
                },
                function (err) {

                });
            


        }, function (error) {
            console.log("line ---> onTokenRefresh2 : " + error);
        });

        window.FirebasePlugin.onMessageReceived(function (message) {
            // if (message.smart_sell_sdk && message.smart_sell_sdk === "true") {
                console.log("line ---> onMessageReceived 1 : " + message.body);
                console.log("line ---> onMessageReceived 2 : " + message.title);
                console.log("line ---> onMessageReceived 3 : " + message.message);
                console.log("line ---> onMessageReceived 4 : " + message.smart_sell_sdk);
                console.log("line ---> onMessageReceived 5 : " + message.action_target);

                var plugin = new SmartSellPlugin();

                plugin.isUserInitialised("9999999999",
                    function (msg) {
                        plugin.handleFcmNotification(message,
                            function (succMsg) {
                                console.log("line ---> onMessageReceived 6 : success");
                            },
                            function (err) {
                                console.log("line ---> onMessageReceived 7 : fail");
                            });
                    },
                    function (err) {

                    });
            // }
        }, function (error) {
            console.log("line ---> onMessageReceived : " + error);
        });

    }
};

app.initialize();

function hideButtons() {
    document.getElementById("smart_sell").style.visibility = "hidden";
    document.getElementById("smart_skill").style.visibility = "hidden";
    document.getElementById("log_out").style.visibility = "hidden";
}

function showButtons() {
    document.getElementById("smart_sell").style.visibility = "visible";
    document.getElementById("smart_skill").style.visibility = "visible";
    document.getElementById("log_out").style.visibility = "visible";
}

function login() {
    isUserInitialised();
}
function initialize() {
    initialData = !initialData;
}

function isUserInitialised() {
    var plugin = new SmartSellPlugin();
    // Here 9999999999 is mobile number
    plugin.isUserInitialised("9999999999",
        function (msg) {
            showButtons();
            console.log("isUserInitialised success");
        },
        function (err) {
            console.log("isUserInitialised fail " + err);
            // generate tokens    
            initialise();
        });
}

function initialise() {
    var smartSellPlugin = new SmartSellPlugin();

    // var baseUrlSmartSell = "https://smartsell.starhealth.in/smartsellapi/public/index.php/v1/";
    // var baseUrlSmartSkill = "https://smartsell.starhealth.in/launchpad-api/public/index.php/v1/";

    var baseUrlSmartSell = "https://smartsell-uat.starhealth.in/smartsellapi/public/index.php/v1/";
    var baseUrlSmartSkill = "https://smartsell-uat.starhealth.in/launchpad-api/public/index.php/v1/";

    // var baseUrlSmartSell = "https://dev.enparadigm.com/starhealth_dev_api/public/index.php/v1/";
    // var baseUrlSmartSkill = "https://dev.enparadigm.com/smartskill_starhealth_dev_api/public/index.php/v1/";
    

    var mobile = "9999999999";
    var userName = "UsersName";
    var email = "email@mail.com";
    var designation = "Developer";
    var language = "en";
    var userType = "1";
    var accessTokenSmartSell = "BqM8ahA8K8o3dDxkYIip";
    var refreshTokenSmartSell = "u9c8v9J8zO6bWkmGzd6y";
    // if(initialData){
        // accessTokenSmartSell = "CUOWlkfjtfjPdqx9RIZ4";
        // refreshTokenSmartSell = "bscnzWCM7Nnp5aXSqaMN";
    // }
    var accessTokenSmartSkill = "cfvtywFKcV6idIdYCu3lTxs8X";
    var refreshTokenSmartSkill = "WNpIOTlnsX90gYQc5I1aYE2eo";

    console.log("accessTokenSmartSell : " + accessTokenSmartSell);
    console.log("refreshTokenSmartSell : " + refreshTokenSmartSell);

    smartSellPlugin.initialise(baseUrlSmartSell,
        baseUrlSmartSkill,
        mobile,
        userName,
        email,
        designation,
        language,
        userType,
        accessTokenSmartSell,
        refreshTokenSmartSell,
        accessTokenSmartSkill,
        refreshTokenSmartSkill,
        function (msg) {
            showButtons();
            console.log("success 2");
        },
        function (err) {
            console.log("fail 2" + err);
        });
}


function smartSkill() {
    var smartSellPlugin = new SmartSellPlugin();
    smartSellPlugin.openSmartSkill(
        function (msg) {
            console.log("msg : " + msg);
        },
        function (err) {
            console.log("error : " + err);
        }
    );
}



function smartSell() {
    var smartSellPlugin = new SmartSellPlugin();
    smartSellPlugin.openSmartSell(
        function (msg) {
            console.log("msg : " + msg);
        },
        function (err) {
            console.log("error : " + err);
        }
    );
}

function logout() {
    var smartSellPlugin = new SmartSellPlugin();
    smartSellPlugin.clearSdkData(
        function (msg) {
            hideButtons();
            console.log("msg : " + msg);
        },
        function (err) {
            console.log("error : " + err);
        }
    );
}



